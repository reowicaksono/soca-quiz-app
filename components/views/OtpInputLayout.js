import { StyleSheet, TextInput } from 'react-native';
import React from 'react';

const OtpInputLayout = ({ placeholder = '', onChangeText = null, value = null, keyboardType = 'default', secureTextEntry = false, ref = null, onChange = null, ...more }) => {
  return (
    <TextInput
      style={styles.formStyle}
      maxLength={1}
      placeholder={placeholder}
      placeholderTextColor="#8C8C8C"
      onChangeText={onChangeText}
      value={value}
      keyboardType={keyboardType}
      secureTextEntry={secureTextEntry}
      ref={ref}
      onChange={onChange}
      {...more}
    />
  );
};

export default OtpInputLayout;

const styles = StyleSheet.create({
  formStyle: {
    fontFamily: 'Avenir Next',
    color: '#fff',
    textAlign: 'center',
    borderColor: '#8C8C8C',
    borderWidth: 1,
    borderRadius: 6,
    width: 41,
    height: 48,
    marginStart: 6,
    marginEnd: 6,
  },
});
