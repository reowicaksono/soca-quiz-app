import { StyleSheet, Text, View } from "react-native";
import React from "react";
import SocaLogo from "../../assets/SocaLogo.svg";
import SearchIcon from "../../assets/Search.svg";
import NotificationIcon from "../../assets/Notification.svg";

const TopBarView = () => {
  return (
    <View style={styles.container}>
      <SocaLogo width={87.44} height={21.35} />
      <View style={styles.iconAction}>
        <SearchIcon style={{ marginEnd: 22 }} />
        <View style={{ flexDirection: "row" }}>
          <NotificationIcon />
          <View style={[styles.notifAlert, { opacity: 1 }]}>
            <Text style={styles.notifAlertText}>1</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default TopBarView;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    marginTop: 45,
    backgroundColor: "#000",
    margin: 24,
    justifyContent: "space-between",
  },
  iconAction: {
    flexDirection: "row",
    alignSelf: "flex-start",
  },
  notifAlert: {
    marginLeft: -9,
    marginTop: -6,
    backgroundColor: "#EB5757",
    borderRadius: 100,
    fontSize: 6,
    width: 13,
    height: 13,
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
  },
  notifAlertText: {
    color: "white",
    fontSize: 6,
    fontFamily: "avenir-next-bold",
  },
});
