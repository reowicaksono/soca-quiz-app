import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import SignUpScreen from '../screens/SignUpScreen';
import SignInScreen from '../screens/SignInScreen';
import ForgotPasswordScreen from '../screens/ForgotPasswordScreen';
import OtpVerificationScreen from '../screens/OtpVerificationScreen';
import NewPasswordScreen from '../screens/NewPasswordScreen';
import HomeScreen from '../screens/HomeScreen';
import SignInBoardingScreen from '../screens/SignInBoardingScreen';
import PlayNavigation from './PlayNavigation';

const Stack = createStackNavigator();

const AuthNavigation = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="signinScreen" component={SignInScreen} options={{ headerShown: false }} />
      <Stack.Screen name="signupScreen" component={SignUpScreen} options={{ headerShown: false }} />
      <Stack.Screen name="forgotPasswordScreen" component={ForgotPasswordScreen} options={{ headerShown: false }} />
      <Stack.Screen name="otpVerificationScreen" component={OtpVerificationScreen} options={{ headerShown: false }} />
      <Stack.Screen name="newPasswordScreen" component={NewPasswordScreen} options={{ headerShown: false }} />
      <Stack.Screen name="signInBoardingScreen" component={SignInBoardingScreen} options={{ headerShown: false }} />
      <Stack.Screen name="playNavigation" component={PlayNavigation} options={{ headerShown: false }} />
    </Stack.Navigator>
  );
};

export default AuthNavigation;

const styles = StyleSheet.create({});
