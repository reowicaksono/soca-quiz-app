import { createStackNavigator } from "@react-navigation/stack";
import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import BottomNavigation from "./BottomNavigation";
import HomePlay from "../screens/HomePlay";
import LobbyRamean from "../screens/LobbyRamean";
import LeaderboardScreen from "../screens/LeaderboardScreen";
import LeaderboardCountdownScreen from "../screens/LeaderboardCountdownScreen";
import LeaderboardCountdownTimScreen from "../screens/LeaderboardCountdownTimScreen";
import LeaderboardTimScreen from "../screens/LeaderboardTimScreen";

const Stack = createStackNavigator()

const LeaderboardNavigation = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="LeaderboardTimScreen" component={LeaderboardTimScreen} options={{ headerShown: false }} />
      <Stack.Screen name="LeaderboardCountdownTimScreen" component={LeaderboardCountdownTimScreen} options={{ headerShown: false }} />
      <Stack.Screen name="LeaderboardCountdownScreen" component={LeaderboardCountdownScreen} options={{ headerShown: false }} />
      <Stack.Screen name="LeaderboardScreen" component={LeaderboardScreen} options={{ headerShown: false }} />
    </Stack.Navigator>
  )
}

export default LeaderboardNavigation
