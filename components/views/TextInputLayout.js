import { StyleSheet, Text, View, TextInput } from 'react-native';
import React from 'react';

const TextInputLayout = ({ label = '', placeholder = '', onChangeText = null, value = null, keyboardType = 'default', secureTextEntry = false, ...more }) => {
  return (
    <View style={styles.formContainer}>
      <Text style={styles.labelStyle}>{label}</Text>
      <TextInput style={styles.formStyle} placeholder={placeholder} placeholderTextColor="#8C8C8C" onChangeText={onChangeText} value={value} keyboardType={keyboardType} secureTextEntry={secureTextEntry} {...more} />
    </View>
  );
};

export default TextInputLayout;

const styles = StyleSheet.create({
  formContainer: {
    marginTop: 10,
    alignSelf: 'stretch',
    marginHorizontal: 38,
  },
  labelStyle: {
    fontFamily: 'avenir-next-reguler',
    color: '#fff',
    fontSize: 14,
  },
  formStyle: {
    fontFamily: 'avenir-next-reguler',
    color: '#fff',
    padding: 10,
    borderColor: '#8C8C8C',
    borderWidth: 1,
    borderRadius: 6,
    marginTop: 8,
  },
});
