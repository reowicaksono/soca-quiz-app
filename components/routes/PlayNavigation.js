import { createStackNavigator } from '@react-navigation/stack';
import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import BottomNavigation from './BottomNavigation';
import HomePlay from '../screens/HomePlay';
import LobbyRamean from '../screens/LobbyRamean';
import LobbySeruan from '../screens/LobbySeruan';
import LobbyDuel from '../screens/LobbyDuel';
import LeaderBoardNavigation from '../routes/LeaderboardNavigation';

const Stack = createStackNavigator();

const PlayNavigation = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="HomeNav" component={BottomNavigation} options={{ headerShown: false }} />
      <Stack.Screen name="HomePlay" component={HomePlay} options={{ headerShown: false }} />
      <Stack.Screen name="LobbyRamean" component={LobbyRamean} options={{ headerShown: false }} />
      <Stack.Screen name="LobbySeruan" component={LobbySeruan} options={{ headerShown: false }} />
      <Stack.Screen name="LobbyDuel" component={LobbyDuel} options={{ headerShown: false }} />
      <Stack.Screen name="LeaderBoardNavigation" component={LeaderBoardNavigation} options={{ headerShown: false }} />
    </Stack.Navigator>
  );
};

export default PlayNavigation;
