import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  Image,
  FlatList,
} from "react-native";
import React, { useState, useEffect, useRef } from "react";
import CloseSVG from "../../assets/image/icon/Close.svg";
import SoundSVG from "../../assets/image/icon/VolumeUp.svg";
import SettingSVG from "../../assets/image/icon/Setting.svg";
import CountSVG from "../../assets/image/icon/CountUser.svg";
import EditSVG from "../../assets/image/icon/Edit.svg";
import { BlurView } from "expo-blur";
import Animated, { SlideInDown, SlideOutDown,  } from "react-native-reanimated";

const LobbyRamean = ({ navigation, route }) => {
  const [username, setUsername] = useState(route.params.username);
  const [countPlayer, setCountPlayer] = useState(0)

  // Wolf Data
  const [dataWolf, setDataWolf] = useState([
    {
      id: 1,
      name: "Asep Balon",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 2,
      name: "Dadang Karbit",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 3,
      name: "Markus Horison",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 4,
      name: "Wawan Gunawan",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 5,
      name: "Asep Balon",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 6,
      name: "Dadang Karbit",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 7,
      name: "Markus Horison",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 8,
      name: "Wawan Gunawan",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
  ]);

  // Horse Data
  const [dataHorse, setDataHorse] = useState([
    {
      id: 1,
      name: "Asep Balon",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 2,
      name: "Dadang Karbit",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 3,
      name: "Markus Horison",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 4,
      name: "Wawan Gunawan",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 5,
      name: "Asep Balon",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 6,
      name: "Dadang Karbit",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 7,
      name: "Markus Horison",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 8,
      name: "Wawan Gunawan",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
  ]);

  //data macan
  const [dataMacan, setDataMacan] = useState([
    {
      id: 1,
      name: "Asep Balon",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 2,
      name: "Dadang Karbit",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 3,
      name: "Markus Horison",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 4,
      name: "Wawan Gunawan",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 5,
      name: "Asep Balon",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 6,
      name: "Dadang Karbit",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 7,
      name: "Markus Horison",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 8,
      name: "Wawan Gunawan",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
  ]);
  //data maung
  const [dataMaung, setDataMaung] = useState([
    {
      id: 1,
      name: "Asep Balon",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 2,
      name: "Dadang Karbit",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 3,
      name: "Markus Horison",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 4,
      name: "Wawan Gunawan",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 5,
      name: "Asep Balon",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 6,
      name: "Dadang Karbit",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 7,
      name: "Markus Horison",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 8,
      name: "Wawan Gunawan",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
    {
      id: 9,
      name: "Wawan Gunawan",
      image: require("../../assets/image/avatar/DadangKarbit.png"),
    },
  ]);

  // count player
  useEffect(() => {
    setCountPlayer(dataWolf.length +
      dataHorse.length +
      dataMacan.length +
      dataMaung.length)
  }, [])

  return (
    <ImageBackground
      source={require("../../assets/image/background/Background.png")}
      style={styles.container}
    >
      {/* Header */}
      <View style={styles.headerContainer}>
        {/* Close Icon */}
        <TouchableOpacity style={styles.languageButtonStyle}>
          <Text style={styles.languageText}>Bahasa Indonesia</Text>
        </TouchableOpacity>

        {/* Right Icon */}
        <View style={styles.headerRightContainer}>
          <TouchableOpacity>
            <SoundSVG />
          </TouchableOpacity>
          <TouchableOpacity style={{ marginHorizontal: 6 }}>
            <SettingSVG />
          </TouchableOpacity>

          <TouchableOpacity onPress={() => navigation.goBack()}>
            <CloseSVG />
          </TouchableOpacity>
        </View>
      </View>

      {/* User */}
      <Animated.View
        entering={SlideInDown.duration(1000)}
        exiting={SlideOutDown.duration(1000)}
      >
        <BlurView intensity={80} tint="dark" style={styles.userContainer}>
          <Image
            source={require("../../assets/image/avatar/User.png")}
            style={{ marginLeft: 26, marginTop: 20, marginBottom: 20 }}
          />
          <View style={styles.userInsideContainer}>
            <Text style={styles.nameText}>{username}</Text>
            <Text style={styles.timText}>Tim Serigala</Text>
          </View>
        </BlurView>

        {/* Container */}
        <BlurView intensity={80} tint="dark" style={styles.bodyContainer}>
          <View style={styles.bodyHeaderContainer}>
            <CountSVG style={{ marginTop: 27 }} />
            <Text style={styles.countText}>
              {countPlayer} 
              {" "}Peserta
            </Text>
          </View>

          {/* Waiting Text */}
          <Text style={styles.waitingText}>
            Menunggu guru untuk memulai ...
          </Text>

          {/* Tim Container */}
          <View style={styles.timListContainer}>
            {/* Tim Top Position */}
            <View style={styles.timSeparationContainer}>
              {/* Tim 1 */}
              <BlurView
                intensity={100}
                tint="dark"
                style={[styles.timContainer, { marginRight: 10 }]}
              >
                <Image
                  source={require("../../assets/image/avatar/Wolf.png")}
                  style={{
                    marginHorizontal: 65,
                    marginTop: 20,
                    marginBottom: 9,
                  }}
                />
                <View style={styles.timNameContainer}>
                  <Text style={styles.timTitleText}>Tim Serigala</Text>
                  <EditSVG style={{ marginLeft: 4 }} />
                </View>
                <Text style={styles.timUserCountText}>
                  {dataWolf.length} Pemain
                </Text>

                {/* Set List if user null or not */}
                <FlatList
                  style={{ marginTop: 15, marginBottom: 21, flexGrow: 0 }}
                  data={dataWolf}
                  keyExtractor={(item) => item.id}
                  ListEmptyComponent={
                    <Text style={styles.emptyUserText}>
                      Menunggu Peserta ...
                    </Text>
                  }
                  renderItem={({ item }) => (
                    <View style={styles.listUserStyle}>
                      <Image source={item.image} style={{height: countPlayer > 20 ? 13.32 : 16.32, width:countPlayer > 20 ? 13.32 : 16.32}} />
                      <Text style={[styles.listNameText, {fontSize: countPlayer > 20 ? 8 : 10}]}>{item.name}</Text>
                    </View>
                  )}
                />
              </BlurView>

              {/* Tim 2 */}
              <BlurView
                intensity={100}
                tint="dark"
                style={[styles.timContainer, { marginLeft: 10 }]}
              >
                <Image
                  source={require("../../assets/image/avatar/Horse.png")}
                  style={{
                    marginHorizontal: 65,
                    marginTop: 20,
                    marginBottom: 9,
                  }}
                />
                <View style={styles.timNameContainer}>
                  <Text style={styles.timTitleText}>Tim Kuda</Text>
                  <EditSVG style={{ marginLeft: 4 }} />
                </View>
                <Text style={styles.timUserCountText}>
                  {dataHorse.length} Pemain
                </Text>

                <FlatList
                  style={{ marginTop: 15, marginBottom: 21, flexGrow: 0 }}
                  data={dataHorse}
                  keyExtractor={(item) => item.id}
                  ListEmptyComponent={
                    <Text style={styles.emptyUserText}>
                      Menunggu Peserta ...
                    </Text>
                  }
                  renderItem={({ item }) => (
                    <View style={styles.listUserStyle}>
                      <Image source={item.image} style={{height: countPlayer > 20 ? 13.32 : 16.32, width:countPlayer > 20 ? 13.32 : 16.32}} />
                      <Text style={[styles.listNameText, {fontSize: countPlayer > 20 ? 8 : 10}]}>{item.name}</Text>
                    </View>
                  )}
                />
              </BlurView>
            </View>

            {/* Tim Bottom Position */}
            <View style={styles.timSeparationContainer}>
              {/* Tim 3 */}
              <BlurView
                intensity={100}
                tint="dark"
                style={[styles.timContainer, { marginRight: 10 }]}
              >
                <Image
                  source={require("../../assets/image/avatar/Macan.png")}
                  style={{
                    marginHorizontal: 65,
                    marginTop: 20,
                    marginBottom: 9,
                  }}
                />
                <View style={styles.timNameContainer}>
                  <Text style={styles.timTitleText}>Tim Macan</Text>
                  <EditSVG style={{ marginLeft: 4 }} />
                </View>
                <Text style={styles.timUserCountText}>
                  {dataMacan.length} Pemain
                </Text>

                <FlatList
                  style={{ marginTop: 15, marginBottom: 21, flexGrow: 0 }}
                  data={dataMacan}
                  keyExtractor={(item) => item.id}
                  ListEmptyComponent={
                    <Text style={styles.emptyUserText}>
                      Menunggu Peserta ...
                    </Text>
                  }
                  renderItem={({ item }) => (
                    <View style={styles.listUserStyle}>
                      <Image source={item.image}  style={{height: countPlayer > 20 ? 13.32 : 16.32, width:countPlayer > 20 ? 13.32 : 16.32}} />
                      <Text style={[styles.listNameText, {fontSize: countPlayer > 20 ? 8 : 10}]}>{item.name}</Text>
                    </View>
                  )}
                />
              </BlurView>

              {/* Tim 4 */}
              <BlurView
                intensity={100}
                tint="dark"
                style={[styles.timContainer, { marginLeft: 10 }]}
              >
                <Image
                  source={require("../../assets/image/avatar/Maung.png")}
                  style={{
                    marginHorizontal: 65,
                    marginTop: 20,
                    marginBottom: 9,
                  }}
                />
                <View style={styles.timNameContainer}>
                  <Text style={styles.timTitleText}>Tim Maung</Text>
                  <EditSVG style={{ marginLeft: 4 }} />
                </View>
                <Text style={styles.timUserCountText}>
                  {dataMaung.length} Pemain
                </Text>

                {/* Set List if user null or not */}
                <FlatList
                  style={{ marginTop: 15, marginBottom: 21, flexGrow: 0 }}
                  data={dataMaung}
                  keyExtractor={(item) => item.id}
                  ListEmptyComponent={
                    <Text style={styles.emptyUserText}>
                      Menunggu Peserta ...
                    </Text>
                  }
                  renderItem={({ item }) => (
                    <View style={styles.listUserStyle}>
                      <Image source={item.image}  style={{height: countPlayer > 20 ? 13.32 : 16.32, width:countPlayer > 20 ? 13.32 : 16.32}}/>
                      <Text style={[styles.listNameText, {fontSize: countPlayer > 20 ? 8 : 10}]}>{item.name}</Text>
                    </View>
                  )}
                />
              </BlurView>
            </View>
          </View>
        </BlurView>
      </Animated.View>
    </ImageBackground>
  );
};

export default LobbyRamean;

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    flex: 1,
    left: 0,
    top: 0,
    bottom: 0,
    right: 0,
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height + 50,
  },
  headerContainer: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    marginLeft: 14,
    marginRight: 19,
    marginTop: 45,
  },
  languageButtonStyle: {
    backgroundColor: "white",
    paddingHorizontal: 16,
    paddingVertical: 10,
    borderRadius: 17,
    alignItems: "center",
    justifyContent: "center",
  },
  languageText: {
    fontFamily: "avenir-next-medium",
    fontSize: 12,
  },
  headerRightContainer: {
    alignItems: "center",
    justifyContent: "space-evenly",
    flexDirection: "row",
  },
  userContainer: {
    alignItems: "flex-start",
    justifyContent: "flex-start",
    flexDirection: "row",
    marginHorizontal: 15,
    marginTop: 47,
    borderRadius: 14,
    borderWidth: 0.88,
  },
  userInsideContainer: {
    marginLeft: 17,
    alignItems: "flex-start",
    marginVertical: 25,
  },
  nameText: {
    color: "white",
    fontSize: 18,
    fontFamily: "avenir-next-medium",
  },
  timText: {
    color: "white",
    fontSize: 12,
    fontFamily: "avenir-next-reguler",
  },
  bodyContainer: {
    alignItems: "center",
    justifyContent: "center",
    marginHorizontal: 15,
    marginTop: 13,
    borderRadius: 14,
    borderWidth: 0.88,
  },
  bodyHeaderContainer: {
    alignSelf: "flex-start",
    alignItems: "center",
    flexDirection: "row",
    marginLeft: 19,
    justifyContent: "center",
  },
  countText: {
    marginTop: 31,
    marginLeft: 7,
    color: "white",
    fontFamily: "avenir-next-medium",
    fontSize: 12,
  },
  waitingText: {
    color: "white",
    fontSize: 14,
    fontFamily: "avenir-next-reguler",
    marginTop: 20,
    marginBottom: 5,
  },
  timListContainer: {
    alignItems: "center",
    justifyContent: "space-evenly",
    alignSelf: "stretch",
    paddingBottom: 19,
  },
  timSeparationContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginHorizontal: 19,
    marginTop: 12,
  },
  timContainer: {
    alignItems: "center",
    borderRadius: 14,
    borderWidth: 0.6,
    height: 222,
    width: 163,
  },
  timNameContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  timTitleText: {
    fontSize: 12,
    fontFamily: "avenir-next-demibold",
    color: "white",
  },
  timUserCountText: {
    color: "#B7B7B7",
    fontSize: 10,
    fontFamily: "avenir-next-reguler",
  },
  listUserStyle: {
    marginBottom: 8,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  listNameText: {
    color: "white",
    fontFamily: "avenir-next-medium",
    fontSize: 10,
    marginLeft: 10,
  },
  emptyUserText: {
    color: "#B7B7B7",
    fontSize: 10,
    fontFamily: "avenir-next-reguler",
    marginTop: 28,
  },
});
