import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Button } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import textStyles from '../styles/TextStyles';
import TopLeftCorner from '../../assets/TopLeftCorner.svg';
import TopRightCorner from '../../assets/TopRightCorner.svg';
import BottomLeftCorner from '../../assets/BottomLeftCorner.svg';
import BottomRightCorner from '../../assets/BottomRightCorner.svg';

const ScanQrTab = () => {
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);

  useEffect(() => {
    const getBarCodeScannerPermissions = async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    };

    getBarCodeScannerPermissions();
  }, []);

  const handleBarCodeScanned = ({ type, data }) => {
    setScanned(true);
    alert(`Bar code with type ${type} and data ${data} has been scanned!`);
  };

  if (hasPermission === null) {
    return <Text style={textStyles.semiBoldText}>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text style={textStyles.semiBoldText}>No access to camera</Text>;
  }

  return (
    // <View style={styles.container}>
    <BarCodeScanner onBarCodeScanned={scanned ? undefined : handleBarCodeScanned} style={styles.cameraFrame}>
      <Text style={[textStyles.semiBoldText, { fontSize: 18 }]}>Join and have fun together!</Text>
      <View style={styles.cornerView}>
        <View style={styles.corner}>
          <TopLeftCorner />
          <TopRightCorner />
        </View>
        <View style={styles.corner}>
          <BottomLeftCorner />
          <BottomRightCorner />
        </View>
        {/* {scanned && <Button title={'Tap to Scan Again'} onPress={() => setScanned(false)} />} */}
      </View>
      <Text style={[textStyles.semiBoldText, { fontSize: 16 }]}>Align the QR Code within the frame</Text>
    </BarCodeScanner>
    // </View>
  );
};

export default ScanQrTab;

const styles = StyleSheet.create({
  // container: {
  //   flex: 1,
  //   backgroundColor: '#333333',
  //   alignItems: 'center',
  // },
  cameraFrame: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  corner: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  cornerView: {
    justifyContent: 'space-between',
    width: 314,
    height: 314,
    marginTop: 80,
    marginBottom: 80,
  },
});
