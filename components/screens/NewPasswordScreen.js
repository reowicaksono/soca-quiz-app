import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, TouchableHighlight, Alert } from 'react-native';
import React, { useState } from 'react';
import LeftArrow from '../../assets/LeftArrow.svg';
import SocaLogo from '../../assets/SocaLogo.svg';
import ButonView from '../views/ButonView';
import textStyles from '../styles/TextStyles';
import TextInputLayout from '../views/TextInputLayout';

const NewPasswordScreen = ({ navigation }) => {
  const [newPassword, setNewPassword] = useState(null);
  const [confirmNewPassword, setConfirmNewPassword] = useState(null);

  const onSaveNewPasswordHandler = () => {
    if (!newPassword && !confirmNewPassword) {
      Alert.alert('Buat password baru kamu dan konfirmasi');
    } else if (!newPassword) {
      Alert.alert('Buat password baru kamu');
    } else if (!confirmNewPassword) {
      Alert.alert('Ketik ulang password baru kamu');
    } else {
      if (newPassword.length < 8) {
        Alert.alert('Password minimal 8 karakter');
      } else {
        if (newPassword === confirmNewPassword) {
          navigation.navigate('signinScreen');
        } else Alert.alert('Password baru kamu tidak sama');
      }
    }
  };

  return (
    <View style={styles.mainContainer}>
      <View style={styles.header}>
        <TouchableHighlight onPress={() => navigation.navigate('forgotPasswordScreen')}>
          <View style={styles.back}>
            <LeftArrow />
            <Text style={[textStyles.semiBoldText, { fontSize: 20, marginStart: 4 }]}>Kembali</Text>
          </View>
        </TouchableHighlight>
      </View>
      <View style={styles.container}>
        <SocaLogo />
        <Text style={[textStyles.semiBoldText, { fontSize: 24, marginTop: 32 }]}>Buat Password Baru</Text>
        <Text style={[textStyles.normalText, { fontSize: 14, marginTop: 8, marginBottom: 24, textAlign: 'center' }]}>Silakan untuk membuat password baru kamu</Text>
        <TextInputLayout label="Password Baru" placeholder="Masukkan password baru kamu" secureTextEntry={true} onChangeText={(val) => setNewPassword(val)} value={newPassword} />
        <TextInputLayout label="Konfirmasi Password" placeholder="Konfirmasi password" secureTextEntry={true} onChangeText={(val) => setConfirmNewPassword(val)} value={confirmNewPassword} />
        <ButonView text="Simpan" onPress={() => onSaveNewPasswordHandler()} />
      </View>
      <View style={styles.footer}>
        <Text style={styles.footerText}>Copyright © 2021 Soca.ai All Reserved</Text>
      </View>
    </View>
  );
};

export default NewPasswordScreen;

const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: 'column',
    flexGrow: 1,
    justifyContent: 'space-between',
    backgroundColor: '#000',
    marginTop: StatusBar.currentHeight || 0,
  },
  header: {
    backgroundColor: '#000',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
  },
  footer: {
    backgroundColor: '#000',
    marginBottom: 26,
  },
  back: {
    marginTop: 60,
    marginStart: 28,
    flexDirection: 'row',
    alignItems: 'center',
  },
  footerText: {
    fontFamily: 'avenir-next-reguler',
    color: '#A6A6A6',
    fontSize: 12,
    textAlign: 'center',
  },
});
