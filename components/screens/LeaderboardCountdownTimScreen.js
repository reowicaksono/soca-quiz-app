import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  Image,
} from "react-native";
import React, { useEffect, useRef, useState } from "react";
import CloseSVG from "../../assets/image/icon/Close.svg";
import SoundSVG from "../../assets/image/icon/VolumeUp.svg";
import { SafeAreaView } from "react-native-safe-area-context";
import { FlatList, ScrollView } from "react-native-gesture-handler";
import UserSVG from '../../assets/image/icon/User.svg';
import CalenderSVG from '../../assets/image/icon/Calendar.svg';
import PlaySVG from '../../assets/image/icon/Play.svg';
import QuestionSVG from '../../assets/image/icon/Question.svg';
import StarLikedSVG from '../../assets/image/icon/StarLiked.svg';
import StarUnlikedSVG from '../../assets/image/icon/StarUnliked.svg';
import LeaderboardPlayerCardView from "../views/LeaderboardPlayerCardView";
import ButonView from "../views/ButonView";
import ButtonViewCustom from "../views/ButtonViewCustom";
import LeaderboardTeamCardView from "../views/LeaderboardTeamCardView";

const LeaderboardCountdownTimScreen = ({ navigation }) => {

  useEffect(() => {
    timeRef.current = 10;


    setTimeout(() => {
      clearAllInterval();
      console.log(timeRef.current);
      console.log("Finished");
      navigation.navigate("LeaderboardScreen")
    }, ((timeRef.current * 1000) + 1500));

    setInterval(() => {
      setTimeLeft(timeRef.current);
      timeRef.current--;

    }, 1000);
    // clearAllInterval();





  }, [timeRef]);

  const clearAllInterval = () => {
    // Get a reference to the last interval + 1
    const interval_id = window.setInterval(function () { }, Number.MAX_SAFE_INTEGER);

    // Clear any timeout/interval up to that id
    for (let i = 1; i < interval_id; i++) {
      window.clearInterval(i);
    }
  }

  const [dataPlayer, setDataPlayer] = useState([
    {
      id: 1,
      playername: 'Zhofron Al Fajr Gunarko Putra ',
      poin: 90,
      correctanswer: 99,
      wronganswer: 1,
      profilepic: require('../../assets/image/avatar/Male-SoftwareEngineer.png'),
      borderstatus: false,
    },
    {
      id: 2,
      playername: 'Kevin',
      poin: 80,
      correctanswer: 8,
      wronganswer: 2,
      profilepic: require('../../assets/image/avatar/Male-Doctor.png'),
      borderstatus: false,
    },
    {
      id: 3,
      playername: 'Yuna',
      poin: 80,
      correctanswer: 8,
      wronganswer: 2,
      profilepic: require('../../assets/image/avatar/Male-Scientist.png'),
      borderstatus: true,
    },
    {
      id: 4,
      playername: 'Fina',
      poin: 70,
      correctanswer: 7,
      wronganswer: 3,
      profilepic: require('../../assets/image/avatar/Male-SoftwareEngineer.png'),
      borderstatus: false,
    },
    {
      id: 5,
      playername: 'Flora',
      poin: 60,
      correctanswer: 6,
      wronganswer: 4,
      profilepic: require('../../assets/image/avatar/Male-Scientist.png'),
      borderstatus: false,
    },
    {
      id: 6,
      playername: 'Shuri',
      poin: 60,
      correctanswer: 6,
      wronganswer: 4,
      profilepic: require('../../assets/image/avatar/Male-Doctor.png'),
      borderstatus: false,
    },
  ]);

  const [dataTim, setDataTim] = useState([
    {
      idTim: 1,
      nama: "The Wolf",
      dataPlayers: dataPlayer,
    },
    // {
    //     idTim : 2,
    //     nama : "The Cat",
    //     dataPlayers : dataPlayer2,
    // },
    // {
    //     idTim : 3,
    //     nama : "The Bunny",
    //     dataPlayers : dataPlayer3,
    // },

  ]);


  const backToPlay = () => {
    // navigation.navigate('HomeNav');
    console.log("Pressed");
  }
  const [timeLeft, setTimeLeft] = useState(0)
  const timeRef = useRef(0)
  // const [time] = useEffect(0);

  return (

    <ImageBackground
      source={require("../../assets/image/background/Background.png")}
      style={styles.containerbackground}
    >

      <View style={styles.containerleaderboard}>
        <Text style={styles.bigCardTitleText}>Skor Tim Kamu</Text>
        <SafeAreaView style={styles.innercontainer}>
          <View style={styles.headerContainer}>
            <View style={styles.headerContainerLeft}>
              <Text style={styles.HomeText}>
                Nama Tim/Peserta
              </Text>
            </View>
            <View style={styles.headerContainerRight}>
              <Text style={styles.HomeText}>
                Poin
              </Text>
            </View>


          </View>

          {/* List Player */}
          <FlatList
            horizontal={false}
            data={dataTim}
            keyExtractor={(item) => item.id}
            renderItem={({ item }) => (
              <LeaderboardTeamCardView
                dataPlayers={item.dataPlayers}
                namaTim={item.nama}
              />
              // <Text></Text>
            )}
          />


          <View style={styles.countdownbox}>
            <Text style={styles.descText}>Leaderboard akan muncul ketika Quiz selesai.</Text>
            <Text style={styles.descText}>Quiz akan berakhir dalam</Text>
            <Text style={styles.timeText}>{timeFormat(timeLeft)}</Text>
          </View>


        </SafeAreaView>
      </View>



    </ImageBackground>
  );
};

export default LeaderboardCountdownTimScreen;

const timeFormat = (timeleft) => {
  // console.log(timeleft);
  minute = Math.floor(timeleft / 60);
  // minute = minute.toFixed(0);
  second = timeleft % 60;
  strmin = "";
  strsec = "";
  if (minute < 10) {
    strmin = "0" + minute;
  } else {
    strmin = "" + minute;
  }

  if (second < 10) {
    strsec = "0" + second;
  } else {
    strsec = "" + second;
  }

  return strmin + ":" + strsec;
}

const styles = StyleSheet.create({
  containerbackground: {
    height: "100%",
    // alignSelf: 'center',
    flexDirection: 'row',

  },
  descText: {
    color: "white",
    fontFamily: "avenir-next-reguler",
    // fontWeight:"bold",
    fontSize: 12,
    verticalAlign: 'middle',
    alignSelf: 'center',
    paddingVertical: 5,
  },
  timeText: {
    // alignSelf: "center",
    color: "white",
    fontFamily: "avenir-next-reguler",
    fontWeight: "bold",
    fontSize: 18,
    verticalAlign: 'middle',
    // backgroundColor:'green',
    alignSelf: 'center',
    paddingVertical: 5,
  },
  innercontainer: {
    //   flex: 1,
    borderRadius: 20,
    // backgroundColor: 'magenta',
    maxHeight: "100%",

  },
  containerleaderboard: {
    // flex: 1,
    //   zIndex:-1,
    backgroundColor: '#00000099',
    // backgroundColor: 'yellow',
    //   maxHeight:200,
    // height:200,
    // opacity:0.5,
    marginHorizontal: 10,
    borderRadius: 20,
    // marginTop:80,
    // alignSelf: 'center',
    // width: '100%',
    alignSelf: 'center',
    // maxHeight: "60%",
  },
  countdownbox: {
    // flex: 1,
    flexDirection: "column",
    height: 230,
    marginTop: 10,
    // alignContent:'center',
    // verticalAlign:'middle',
    // marginVertical:20,
    // maxHeight: 200,
    backgroundColor: '#00000099',
    // backgroundColor: 'blue',
    justifyContent: 'center',
    marginHorizontal: 20,
    borderRadius: 20,
    // opacity:0.2,
    marginBottom: 20,


  },
  headerContainer: {
    // flex:1,
    flexDirection: 'row',
    //   alignItems: 'center',
    //   justifyContent: "flex-start",
    marginLeft: 10,
    marginRight: 20,
    paddingHorizontal: 15,
    //   width:"100%",
    //   backgroundColor:'blue',
    //   alignContent: "flex-end",
    //   marginBottom: 24,
  },
  headerContainerRight: {
    flex: 1,
    flexDirection: 'row-reverse',
    //   alignContent: "flex-end",
    //   alignSelf: "flex-end",
    //   backgroundColor:'red',
    //   width:'50%',
    //   marginRight: 20,
  },
  headerContainerLeft: {
    flex: 1,
    flexDirection: 'row',
    // justifyContent: "space-between",
    // width:'50%',
    // backgroundColor:'green',
    // marginRight: 20,
  },
  searcBarContainer: {
    flexDirection: 'row',
    backgroundColor: '#222222',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 10,
    marginTop: 23,
    marginLeft: 25,
    marginRight: 24,
    borderRadius: 4,
  },
  searchTextInputStyle: {
    marginLeft: 11,
    color: 'white',
    fontFamily: 'avenir-next-demibold',
    width: '90%',
    fontSize: 12,
  },
  bigCardContainer: {
    flexDirection: 'row',
    backgroundColor: '#222222',
    alignContent: 'flex-end',
    justifyContent: 'flex-start',
    marginLeft: 24,
    marginTop: 26,
    marginRight: 25,
    borderRadius: 10,
  },
  bigCardRightContentContainer: {
    marginRight: 19,
    width: 365,
  },
  bigCardTitleText: {
    fontFamily: 'avenir-next-medium',
    color: 'white',
    //   width: 200,
    marginTop: 15,
    marginLeft: 20,
    fontWeight: "bold",
    marginBottom: -25,
    fontSize: 16,
    //   marginRight: 33,
    //   marginTop: 27,
  },
  bigCardDescText: {
    color: '#9D9D9D',
    width: 172,
    fontFamily: 'avenir-next-reguler',
    fontSize: 12,
    marginTop: 7,
  },
  learnButtonStyle: {
    backgroundColor: '#BC3BF2',
    alignSelf: 'flex-end',
    justifyContent: 'center',
    borderRadius: 22,
    marginRight: 19,
    marginTop: 14,
    marginBottom: 15,
  },
  learnButtonText: {
    color: 'white',
    paddingVertical: 5,
    paddingRight: 10,
    paddingLeft: 12,
    fontSize: 10,
    fontFamily: 'avenir-next-demibold',
  },
  HomeText: {
    // flex:1,
    fontSize: 14,
    fontFamily: 'avenir-next-reguler',
    color: 'white',
    marginTop: 10,
    marginLeft: 10,
  },
  cardAtHomeContainer: {
    backgroundColor: '#222222',
    flexDirection: 'row',
    marginLeft: 8,
    borderRadius: 10,
  },
  cardAtHomeLeftSideContainer: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginTop: 15,
    marginLeft: 17,
  },
  atHomeLeftSideText: {
    fontSize: 10,
    fontFamily: 'avenir-next-reguler',
    color: 'white',
    marginTop: 7,
    marginBottom: 15,
    width: 123,
  },
  cardAtHomeRightSideContainer: {
    marginRight: 12,
    marginTop: 14,
    alignItems: 'flex-end',
    justifyContent: 'space-between',
  },
  cardAthHomeUserContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  userText: {
    color: 'white',
    fontFamily: 'avenir-next-reguler',
    fontSize: 8,
  },
  joinButtonStyle: {
    backgroundColor: '#BC3BF2',
    alignSelf: 'flex-end',
    justifyContent: 'center',
    marginBottom: 11,
    borderRadius: 22,
  },
  joinButtonText: {
    fontFamily: 'avenir-next-medium',
    fontSize: 8,
    color: 'white',
    paddingHorizontal: 12,
    paddingVertical: 3,
  },
  quizContainer: {
    marginLeft: 6,
    borderRadius: 5,
    backgroundColor: '#222222',
  },
  quizTitleText: {
    fontFamily: 'avenir-next-demibold',
    color: 'white',
    fontSize: 9,
    marginLeft: 7,
    marginTop: 6,
    width: 76,
  },
  quizFooterContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  quizFooter: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 6,
    marginBottom: 9,
  },
  quizText: {
    color: '#C1C1C1',
    fontSize: 6,
    fontFamily: 'avenir-next-reguler',
    marginLeft: 2,
  },
  adventureContainer: {
    flexDirection: 'row',
    marginTop: 10,
    marginLeft: 9,
    backgroundColor: '#222222',
    borderRadius: 10,
    marginBottom: 84,
  },
  adventureLeftContainer: {
    marginTop: 26,
    marginLeft: 23,
    marginBottom: 17,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  adventureTitleText: {
    fontFamily: 'avenir-next-reguler',
    fontSize: 14,
    color: 'white',
    width: 181,
  },
  adventureFooterContainer: {
    marginTop: 20,
    flexDirection: 'row',
    alignItem: 'center',
    justifyContent: 'flex-start',
  },
  adventureFooter: {
    flexDirection: 'row',
    alignItem: 'center',
    justifyContent: 'center',
  },
  adventureText: {
    color: '#C1C1C1',
    fontSize: 10,
    fontFamily: 'avenir-next-reguler',
    marginLeft: 3,
    marginRight: 11,
  },
});
